# Backports for RHEL

This repo contains the scripts and CI jobs for building newer versions of
different pieces of software on **RHEL**, to be used by RHEL builders
container images at **CKI**.

## Software backported


* **Bash 4.4**: Only in RHEL6.
* **OpenSSL 1.0.2u**: Only in RHEL6.
* **Python 3.9.13**
* **SQLite 3.38.5**: Only in RHEL6.

## Architectures

* **RHEL6**: amd64
* **RHEL7**: amd64, ppc64le, s390x 
* **RHEL8**: amd64, arm64, ppc64le, s390x

The software compiled here will be stored as artifacts to be used at
build time in [CKI containers repository].


Job names follow this rule `build-<rhel_version>-<architecture>-<backported_software>`.

To get any artifact you can follow the Gitlab documentation to [download artifacts], for example
to download the python version for rhel8 in arm64, you can do it something similar to:

```bash
 curl --output-file python.tgz --location \
    https://gitlab.com/api/v4//projects/36546257/jobs/artifacts/main/raw/python.tgz?job=build-rhel8-arm64-python
```

[CKI containers repository]: https://gitlab.com/cki-project/containers/
[download artifacts]: https://docs.gitlab.com/ee/api/job_artifacts.html#download-the-artifacts-archive
