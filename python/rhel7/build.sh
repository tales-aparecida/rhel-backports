#!/bin/bash
set -euo pipefail

source common_functions.sh

RPM_PACKAGE_MANAGER="yum"
RPM_PACKAGES=(
    bzip2-devel
    gcc
    gcc++ 
    libffi-devel
    libgcrypt-devel
    libuuid-devel
    make 
    ncurses-devel
    openssl-devel
    perl
    readline-devel
    sqlite-devel
    tar
    xz-devel
    zlib-devel
)
SOURCES_DIR="/usr/src"
ARTIFACT_NAME="python"
ARTIFACT_SRC="/usr/local"

install_packages "${RPM_PACKAGE_MANAGER}" "${RPM_PACKAGES[@]}"
# shellcheck disable=SC2154
download_and_uncompress_python "${PYTHON_VERSION}" "${SOURCES_DIR}"
# shellcheck disable=SC2154
cd "${SOURCES_DIR}/Python-${PYTHON_VERSION}/"
configure_and_install ""
store_artifact "${ARTIFACT_NAME}" "${ARTIFACT_SRC}"
