#!/bin/bash
set -euo pipefail

# shellcheck source-path=SCRIPTDIR
source common_functions.sh

RPM_PACKAGE_MANAGER="yum"
RPM_PACKAGES=(
    bzip2-devel
    gcc
    libffi-devel
    libgcrypt-devel
    libuuid-devel
    ncurses-devel
    perl
    readline-devel
    sqlite-devel
    tar
    xz-devel
    zlib-devel
)
SOURCES_DIR="/usr/src"
ARTIFACT_NAME="python"
ARTIFACT_SRC="/usr/local"


install_packages "${RPM_PACKAGE_MANAGER}" "${RPM_PACKAGES[@]}"

tar -zxf openssl.tgz -C /usr/local/
tar -zxf sqlite.tgz -C /usr/local/
echo "/usr/local/ssl/lib/" > /etc/ld.so.conf.d/local.conf
echo "/usr/local/lib/" >> /etc/ld.so.conf.d/local.conf
ldconfig 

# shellcheck disable=SC2154
download_and_uncompress_python "${PYTHON_VERSION}" "${SOURCES_DIR}"
# shellcheck disable=SC2154
cd "${SOURCES_DIR}/Python-${PYTHON_VERSION}/"
configure_and_install ""
store_artifact "${ARTIFACT_NAME}" "${ARTIFACT_SRC}"
